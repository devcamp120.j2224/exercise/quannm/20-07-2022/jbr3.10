package com.devcamp.jbr310.jbr310;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowController {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowColor() {
        ArrayList<String> listColor = new ArrayList<String>();

        Rainbow red = new Rainbow("red");
        Rainbow orange = new Rainbow("orange");
        Rainbow yellow = new Rainbow("yellow");
        Rainbow green = new Rainbow("green");
        Rainbow blue = new Rainbow("blue");
        Rainbow indigo = new Rainbow("indigo");
        Rainbow violet = new Rainbow("violet");

        listColor.add(red.toString());
        listColor.add(orange.toString());
        listColor.add(yellow.toString());
        listColor.add(green.toString());
        listColor.add(blue.toString());
        listColor.add(indigo.toString());
        listColor.add(violet.toString());

        return listColor;
    }
    
}
