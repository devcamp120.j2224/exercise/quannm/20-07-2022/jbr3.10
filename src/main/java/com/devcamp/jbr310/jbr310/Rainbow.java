package com.devcamp.jbr310.jbr310;

public class Rainbow {
    private String color;
    
    public Rainbow(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return getColor();
    }
}
